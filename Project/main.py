#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import urllib2, urllib, json
import jinja2, webapp2

import os
import logging

JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

####### Reading API ########       
def pretty(obj):
    return json.dumps(obj, sort_keys=True, indent=2)

def safeGet(url):
    try:
        return urllib2.urlopen(url)
    except urllib2.HTTPError, e:
        print "The server couldn't fulfill the request." 
        print "Error code: ", e.code
    except urllib2.URLError, e:
        print "We failed to reach a server"
        print "Reason: ", e.reason
    return None

def recipeon(baseurl = 'http://api.recipeon.us/2.0/recipe/suggest',
    developerkey = '5660adeba00a6d3b0400000c',
    From="0",
    count="1",
    params=''
    ):
    if format == "json": params["nojsoncallback"]=True
    url = baseurl + "/" + developerkey + "/" + From + "/" + count + "/" + params + "/"
    return safeGet(url)
    
def get_ingredients(searchlist):
    #searchlist=list of ingredients as strings
    ingredients=""
    try:
        for x in range(len(searchlist)):
            ingredients+=searchlist[x]
            if x is not int(len(searchlist)-1):
                ingredients+="&"
        search = json.loads(recipeon(params=ingredients).read())
        return search
    except:
        return None
#print recipeon()
#print pretty(get_ingredients(["chicken", "onion", "pork", "mushrooms", "garlic", "basil", "almond", "flour", "fillet"]))
#print pretty(json.loads(safeGet("http://api.recipeon.us/2.0/recipe/suggest/5660adeba00a6d3b0400000c/0/1/onion").read()))

####### Reading only ingredients the user has######## 
#get_ingredients(searchlist)["response"]["numFound"]
staples=["butter", "water", "salt", "milk", 'pepper', 'sugar', 'flour']

def get_only_ingredient(searchlist, recipe_num):
    """recipe_num=number of "hit" off the ingredient search as an integer
        ex: ingredient search reveals 10 recipes, recipe_num=2 is the second recipe returned in the search"""
    try:
        search = get_ingredients(searchlist)["response"]["docs"][int(recipe_num)]
        print search
        for item in searchlist:
            if item in search["ingredient"]:
                search["ingredient"].remove(item)           
        if len(search["ingredient"])==0:
            return get_ingredients(searchlist)["response"]["docs"][int(recipe_num)]
    except:
        return None
print get_only_ingredient(["chicken", "onion", "pork", "mushrooms", "garlic", "basil", "almond", "flour", "fillet"], 0)
class Recipe():
    def __init__(self, searchlist, recipe_num):
        self.title = get_only_ingredient(searchlist, recipe_num)["title"]
"""     self.ingredients = get_only_ingredient(searchlist, recipe_num)["ingredient"]
        self.photo = get_only_ingredient(searchlist, recipe_num)["img"]
        self.url = get_only_ingredient(searchlist, recipe_num)["url"]
        self.commentcount = photo_info["comments"]["_content"]
        self.num_views = photo_info["views"]
        self.url = photo_info["urls"]["url"][0]["_content"]
    def __str__(self):
        s = ''
        s += '~~~ ' + self.title.encode('utf-8') + ' ~~~\n'
        s += 'author: ' + self.author.encode('utf-8') + '\n'
        s += 'number of tags: ' + str(len(self.tags)) + '\n'
        s += 'views: ' + self.num_views.encode('utf-8') + '\n'
        s += 'comments: ' + self.commentcount.encode('utf-8')
        return s

def output(dic):
    thumbnail="https://farm%s.staticflickr.com/%s/%s_%s_t.jpg" %(str(dic["farm"]), str(dic["server"]), str(dic["id"]), str(dic["secret"]))
    pic_name=Photo(dic).title.encode('utf-8')
    pic_url=Photo(dic).url
    return thumbnail, pic_name, pic_url

class GreetHandler(webapp2.RequestHandler):
    def get(self):
        vals = {}
        vals['page_title']="Flickr Search"
        template = JINJA_ENVIRONMENT.get_template('greetform.html')
        self.response.write(template.render(vals))
    def post(self):
        vals = {}
        search = self.request.get('search', "husky")
        go = self.request.get('gobtn') 
        vals['page_title']="Search Results"
        logging.info(search)
        logging.info(go)
        try:
            # if form filled in, greet them using this data
            ids = get_photo_ids(search, n=5)
            vals['photos'] = [output(get_photo_info(x)) for x in ids]
            template = JINJA_ENVIRONMENT.get_template('greetresponse.html')
            self.response.write(template.render(vals))
            logging.info('search= '+search)
        except:
            #if not, then show the form again with a correction to the user
            vals['prompt'] = "What do you want to search?"
            template = JINJA_ENVIRONMENT.get_template('greetform.html')
            self.response.write(template.render(vals))"""
    

# for all URLs except alt.html, use MainHandler
application = webapp2.WSGIApplication([ \
                                      ('/.*', GreetHandler)
                                      ],
                                     debug=True)"""
