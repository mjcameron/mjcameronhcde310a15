
### you will need this function later in the homework
def stripWordPunctuation(word):
    return word.strip(".,()<>\"\\'~?!;*:[]-+")


print "== part 3 =="
### part 3: fieldType() function
# In hw3feed.txt, note that there are both posts and comments in this feed. There are also posters.
# These lines each start with post:, comment:, and from: respectively.
# You are probably thinking "wow, it would be really helpful if we had a function to tell the type of line wit
# which I am working." The good news is that now you will write one. Find the part 3 line in hw3-feedprocessor.py
# We have included some starter code to define a function fieldType(). This function should take a line as
# parameter and return the field type (either post, comment, or from).

def fieldType(line):
    line = line.split()
    return stripWordPunctuation(line[0])

# You can uncomment and test your function with these lines
print fieldType("from: Sean")
print fieldType("post: Hi everyone")
print fieldType("comment: Thanks!")

print "== part 4 =="
### part 4: printing users

# Your job is to extract the names and print them out, exactly as it is shown in
# the screenshot in the PDF file. Hint: if you want to remove "from:"  you can
# use string slicing operations or the replace method.

# You may use the fieldType() function but you do not have to. You may also define
# another function, such as fieldContents() to help you but that optional. 

# Duplicate names are expected for this part!

fname = "hw3feed.txt"
f = open(fname,'r')

def user(line):
    line = line.split()
    if stripWordPunctuation(line[0]) == "from" and len(line) == 3:
        return str(line[1]) + " " + str(line[2])
    if stripWordPunctuation(line[0]) == "from" and len(line) == 4:
        return str(line[1]) + " " + str(line[2]) + " " + str(line[3])
for line in f:
    if type(user(line)) == str:
        print user(line)
f.close()
        
print "== part 5 =="
### part 5: counting poster contribution frequency
# see the instructions in the PDF file. They are easier to follow with
# formatting

post_count = {}
f = open(fname,'r')

# read in and count the number of posts per user

for line in f:
    if user(line) in post_count.keys():
        post_count[user(line)] = post_count[user(line)] + 1
    else:
        post_count[user(line)] = 1

# print the number of times each user posted

for key in post_count:
    print str(key) + ": " + str(post_count[key])
f.close()

print "== part 6 =="
### part 6: counting word frequency
# This is similar to post count in part 6 and you might
# even re-use some of your code. Count the number of
# times each word appears in all posts, but *not* comments

# use the stripWordPunctuation() function to get rid of punctuation.
# note that it is not perfect so some extra punctuation may remain.

post_word_count = {}
f = open(fname,'r')

# read in and count of times each word appeared

for line in f:
    if fieldType(line) == "post":
        line = line.split()
        for word in line:
            word = stripWordPunctuation(word.lower())
            post_word_count[word] = post_word_count.get(word, 0) + 1

# print the number of times each word appeared

for key in post_word_count:
    print str(key) + ": " + str(post_word_count[key])
f.close()

print "== part 7 =="
### part 7: counting word frequency in comments and posts
# for this part, write a function, wordFreq() that will return
# the word frequency in either posts or comments as a dictionary

# As parameters, it must take a file name and the field type
# (either post or comment) as a parameter

# for example, if I want to get a dictionary of word counts in
# the posts in hw3feed.txt, I should be able to call:
# wordFreq('hw3feed.txt','post')

# you can use your code from part 6 as a starting point, or
# if you wrote part 6 using a function, you may simply edit it
# to meet the requirements for this part.

# uncomment and begin editing from the next line:
def wordFreq(filename, post_type):
    word_count = {}
    f = open(filename, "r")
    for line in f:
        if fieldType(line) == post_type:
            line = line.split()
            for word in line:
                word = stripWordPunctuation(word.lower())
                word_count[word] = word_count.get(word, 0) + 1
    return word_count
    f.close()

# to test ,you can uncomment and run these  lines:
if wordFreq(fname,'post')["anyone"] == 4 and wordFreq(fname,'post')["eclipse"] == 11:
    print "Looks like wordFreq() works fine for posts"
else:
    print "We got some errors with wordFreq() for posts."
 
if wordFreq(fname,'comment')["file"] == 7 and wordFreq(fname,'comment')["if"] == 23:
    print "Looks like wordFreq() works fine for comments"
else:
    print "We got some errors with wordFreq() for comments."

