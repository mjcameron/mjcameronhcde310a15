# Macauley Cameron, form A

import urllib, urllib2, webbrowser, json

def pretty(obj):
    return json.dumps(obj, sort_keys=True, indent=2)

def getFlights(user="1", params={}):
    baseurl="http://hcde310demo.appspot.com/api/v2?method=flights.flights"
    params["user"]=user
    url = baseurl + "&" + urllib.urlencode(params)
    try:
        return json.loads(urllib2.urlopen(url).read())
    except urllib2.HTTPError, e:
        print "The server couldn't fulfill the request." 
        print "Error code: ", e.code
    except urllib2.URLError, e:
        print "We failed to reach a server"
        print "Reason: ", e.reason
    return None

def getAirports(airportsList):
    baseurl="http://hcde310demo.appspot.com/api/v2?method=flights.airportInfo"
    s="airports="
    for code in airportsList:
        s += code + ","
    url = baseurl + "&" + s
    try:
        return json.loads(urllib2.urlopen(url).read())
    except urllib2.HTTPError, e:
        print "The server couldn't fulfill the request." 
        print "Error code: ", e.code
    except urllib2.URLError, e:
        print "We failed to reach a server"
        print "Reason: ", e.reason
    return None
#print getAirports(['PDX', 'SEA', 'NON'])

class Flight():
    def __init__(self, flight_dict):
        self.fromAirport=flight_dict["fromAirport"]
        self.toAirport=flight_dict["toAirport"]
        self.date=flight_dict["date"]
        self.aircraftType=flight_dict["aircraftType"]
#print Flight(getFlights()["flights"][0]).toAirport

class Airport():
    def __init__(self, airport_dict):
        self.iata=airport_dict["iata"]
        self.name=airport_dict["airportName"]
        self.city=airport_dict["city"]
        self.country=airport_dict["country"]
        self.latitude=airport_dict["location"]["latitude"]
        self.longitude=airport_dict["location"]["longitude"]
#print Airport(getAirports(['PDX', 'SEA', 'NON'])["airports"][0]).city

print "\n= 2.1 ="
f=getFlights("1", {"fromDate":"01-01-2006", "toDate":"12-31-2006"})
for x in f["flights"]:
    v = Flight(f["flights"][f["flights"].index(x)])
    print v.date, v.toAirport + "-" + v.fromAirport, v.aircraftType
   
print "\n= 2.2 ="
chicago=[]
for x in f["flights"]:
    v = Flight(f["flights"][f["flights"].index(x)])
    if v.fromAirport == "ORD":
        chicago.append(1)
    if v.toAirport == "ORD":
        chicago.append(1)       
print "ORD flights:", len(chicago)

print "\n= 2.3 ="
freq=getFlights("1", {"fromDate":"01-01-2006"})
ap={}
for x in freq["flights"]:
    d = Flight(freq["flights"][freq["flights"].index(x)])
    ap[d.toAirport] = ap.get(d.toAirport, 0) + 1
airports=ap.keys()
airports.sort(key= lambda a: ap[a], reverse=True)
for i in airports[:10]:
    print str(airports.index(i)+1) + ".", i, ap[i]

print "\n= 2.4 ="
t=getFlights("1", {"fromDate":"01-01-2010"})
p={}
for x in t["flights"]:
    d = Flight(t["flights"][t["flights"].index(x)])
    p[d.aircraftType] = p.get(d.aircraftType, 0) + 1
types=p.keys()
types.sort(key= lambda a: p[a], reverse=True)
for i in types:
    print str(i)+":", p[i]

print "\n= 2.5 ="


print "\n= 2.6 ="

