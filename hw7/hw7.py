import urllib, urllib2, webbrowser, json
from __builtin__ import list

### Utility functions you may want to use
def pretty(obj):
    return json.dumps(obj, sort_keys=True, indent=2)


def safeGet(url):
    try:
        return urllib2.urlopen(url)
    except urllib2.HTTPError, e:
        print "The server couldn't fulfill the request." 
        print "Error code: ", e.code
    except urllib2.URLError, e:
        print "We failed to reach a server"
        print "Reason: ", e.reason
    return None

#### Main Assignment ##############

## Don't forget, you need to get your own api_key from Flickr, following the
#procedure in session 10 slides. Put it in the file flickr_key.py
# Then, UNCOMMENT the api_key line AND the params['api_key'] line in the function below.
import flickr_key
def flickrREST(baseurl = 'https://api.flickr.com/services/rest/',
    method = 'flickr.photos.search',
    api_key = 'ec3bec310497be7b61477f1678af1821',
    format = 'json',
    params={},
    printurl = False
    ):
    params['method'] = method
    params['api_key'] = api_key
    params['format'] = format
    if format == "json": params["nojsoncallback"]=True
    url = baseurl + "?" + urllib.urlencode(params)
    if printurl:
        print url
    else:
        return safeGet(url)

### This is where you should start filling in code...

## Building block 1 ###
# Define a function called get_photo_ids() which uses the flickr API to search
# for photos with a given tag, and return a list of photo IDs for the
# corresponding photos. Use a list comprehension to generate the list.
# Hints: Use flickrREST(). You may wish to use print & pretty() to inspect
#       the data returned by flickr to figure out what fields to extract. You
#       may find useful code to copy and edit in s10.py, but make sure you
#       understand what it's doing!
#
#       flickrREST() defaults to the flickr.photos.search method, documented
#       at https://www.flickr.com/services/api/flickr.photos.search.html
#       This method will work for building block 1, but see the documentation
#       for what parameters you might pass.
#
# Inputs:
#   tag: a tag to search for
#   n: the number of search results per page (default value should be 100)
# Returns: a list of (at most) n photo ids, or None if an error occured
def get_photo_ids(tag, n=100):
    try:
        search = json.loads(flickrREST(params={"tags":tag,"per_page":n}).read())
        if len(search["photos"]["photo"]) > 0:
            return [search["photos"]["photo"][x]["id"] for x in range(n)]
    except:
        return None

## Building block 2 ###
## Define a function called get_photo_info() which uses the flickr API to
# get information about a particular photo id.  The information should be
# returned as a python dictionary Hint: use flickrREST and the flickr API method
# flickr.photos.getInfo, documented at
# http://www.flickr.com/services/api/flickr.photos.getInfo.html
# Inputs:
#   photo_id: the id of the photo you to get information about
# Returns: a dictionary with photo info, or None if an error occurred
def get_photo_info(photo_id):
    try:
        photo = json.loads(flickrREST(method = "flickr.photos.getInfo", params = {"photo_id":photo_id}).read())
        if "photo" in photo.keys():
            return photo["photo"]
    except:
        return None

## Building block 3 ###
## Define a class called Photo to represent flickr photos
# It should have at least three methods:
# (a) a constructor (init__())
# (b) a string representation (__str__())
# (c) a function that opens the photo in your web browser (open_url())
class Photo():
## (a) __init__():
# The constructor (remember, the __init__() method is called the constructor)
# should take a dictionary representing photo info  and initialize
# four instance variables:
#  -title: the title of the photo (Use "_content"!)
#  -author: the user that posted the photo (use username!)
#  -userid: the user nsid (####@N##, for example)
#  -tags: a list of tags (strings) associated with the photo (Use "_content"!)
#  -commentcount: a count with the number of comments on the photo
#  -num_views: the number of times the photo was viewed
#  -url: the location of the photo on flickr
# Your constructor should use a list comprehension to create the tags list.
# Hint: You may wish to use print and pretty() to determine which fields in
#       the photo info dictionary to extract.
#       If a field needs to be converted to a number from a different type
#       (i.e. num_views), be sure to do that in the body of the constructor.
    def __init__(self, photo_info):
        self.title = photo_info["title"]["_content"]
        self.author = photo_info["owner"]["username"]
        self.userid = photo_info["owner"]["nsid"]
        self.tags = [photo_info["tags"]["tag"][x]["_content"] for x in range(len(photo_info["tags"]["tag"]))]
        self.commentcount = photo_info["comments"]["_content"]
        self.num_views = photo_info["views"]
        self.url = photo_info["urls"]["url"][0]["_content"]

## (b) __str__()
# The __str__() method should return a string with the following format:
#   ~~~ <TITLE> ~~~
#   author: <AUTHOR>
#   number of tags: <NUMBER OF TAGS IN TAGS INSTANCE VARIABLE>
#   views <NUMBER OF VIEWS>
#   comments: <NUMBER OF COMMENTS>
# Look at HW 5 if you need reminders about doing this.
# Tip: use .encode('utf-8') on the title and author in case either
#          string contains unicode characters (strings that look like
#          u'something')
    def __str__(self):
        s = ''
        s += '~~~ ' + self.title.encode('utf-8') + ' ~~~\n'
        s += 'author: ' + self.author.encode('utf-8') + '\n'
        s += 'number of tags: ' + str(len(self.tags)) + '\n'
        s += 'views: ' + self.num_views.encode('utf-8') + '\n'
        s += 'comments: ' + self.commentcount.encode('utf-8')
        return s
        

## (c) open_url()
# The open_url() method should take your web browser to the location
# specified by the url instance variable.
# The variable called url has a string value stored in it, a url, or address.
# The open_url method of this class shoud use that url: it should open
#     that URL in a web browser.
    def open_url(self):
            webbrowser.open(self.url)

if __name__ == '__main__':
    ### Testing your building blocks
    print '\n\nTesting your building blocks\n------------'
    # test get_photo_ids() with the following line of code, which will give
    # note the ids you get may be different than what's in the sample screenshot.
    print get_photo_ids('hamster', n=4)

    # Test get_photo_info() with the following two lines of code:
    pd = get_photo_info(5140736446)
    print pretty(pd)

    # Test your Photo class with the following lines of code: Check the format
    # of your output against sample output in the PDF file, and make adjustments
    # to your __init__ and __str__ methods as needed
    po = Photo(pd)
    print po
    print po.tags
    #po.open_url()


    ### Part 1 #########
    # Use your get_photo_ids function to get a list of 100 photo ids with a tag of your choosing.
    # Convert the list of ids into a list of Photo objects using a list comprehension
    # Pro tip: You may want to start out with the first 10 or 20 photos while
    #          testing. It takes a while to run.
    # Note: nothing gets printed out in this part. But you might want to do some
    # printing to check if it's working

    objects = [Photo(get_photo_info(x)) for x in get_photo_ids("husky")]

    ### Part 2 #########
    # (a) Order the photo objects by number of views. Print the 5 most viewed photos
    # and open the top one in your web browser using open_url()
    print "\nTop Five Photos by Views"
    print "------------"
    
    objects = sorted(objects, key=lambda x: int(x.num_views), reverse=True)
    for x in objects[:5]:
        print x
    #objects[0].open_url()   

    # (b) Order the photo objects by number of tags. Print the 5 most tagged photos
    # and open the top one in your web browser using open_url()
    print "\nTop Five Photos by Number of Tags"
    print "------------"
    
    objects = sorted(objects, key=lambda x: int(len(x.tags)), reverse=True)
    for x in objects[:5]:
        print x
    #objects[0].open_url()
    
    # (c) Order the photo objects by number of tags. Print the 5 most commented photos
    # and open the top one in your web browser using open_url()
    # NOTE: it is completely possible that you will have no photos with comments in your data set.
    print "\nTop Five Photos by Number of Comments"
    print "------------"
    
    objects = sorted(objects, key=lambda x: int(x.commentcount), reverse=True)
    for x in objects[:5]:
        print x
    #objects[0].open_url()

    # Based on the photos you get, which do you think is a better way to find
    # good photos, the ones with the most views or the most tags?
    print "------------"

    ### Part 3 #########
    # Compute the total number of views received by each author in the photo
    # object list.  Then, print out the username of the author along with
    # the total number of views their photos had received, for the top five
    # users, ranked in order of number of views, in the following format:
    # (1) Johnny 5: 1221
    # (2) Aravind: 134
    # (3) Sid: 120
    # (4) Korn: 113
    # (5) Stephanie: 108
    # (6) Cole: 104
    # (7) Ben: 98
    # (8) Tsuki: 54
    # (9) Joanna: 12
    # (10) Sean: 1
    print "\nTop ten authors by number of views"
    print "------------"
    
    auth_views_dict = {}
    for x in objects:
        auth_views_dict[x.author] = auth_views_dict.get(x.author, 0) + int(x.num_views)
    
    auth_views_list = auth_views_dict.keys()
    auth_views_list.sort(key= lambda a: auth_views_dict[a], reverse=True)
    
    for a in auth_views_list[:10]:
        print "(%d) %s: %d" % (auth_views_list.index(a) + 1, a, auth_views_dict[a])  
    