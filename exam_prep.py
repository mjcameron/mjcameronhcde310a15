import urllib, urllib2, webbrowser, json
def pretty(obj):
    return json.dumps(obj, sort_keys=True, indent=2)


def getBestSellers(listname, params={}):
    baseurl="http://api.nytimes.com/svc/books/v2/lists.json?api-key=500948cb779a6b477d53f43f0a82630d:16:73569763"
    params["list-name"]=listname
    url = baseurl + "&" + urllib.urlencode(params)
    try:
        urllib2.urlopen(url)
        return json.loads(urllib2.urlopen(url).read())
    except urllib2.HTTPError, e:
        print "The server couldn't fulfill the request." 
        print "Error code: ", e.code
    except urllib2.URLError, e:
        print "We failed to reach a server"
        print "Reason: ", e.reason
    return None
#print pretty(getBestSellers("Mass-Market-Paperback"))
def searchBestSeller(params={}):
    baseurl="http://api.nytimes.com/svc/books/v2/lists.json?api-key=500948cb779a6b477d53f43f0a82630d:16:73569763"
    url = baseurl + "&" + urllib.urlencode(params)
    try:
        urllib2.urlopen(url)
        return json.loads(urllib2.urlopen(url).read())
    except urllib2.HTTPError, e:
        print "The server couldn't fulfill the request." 
        print "Error code: ", e.code
    except urllib2.URLError, e:
        print "We failed to reach a server"
        print "Reason: ", e.reason
    return None
class Book():
    def __init__(self, book_dict):
        self.title=book_dict["book_details"][0]["title"]
        self.author=book_dict["book_details"][0]["author"]
        self.description=book_dict["book_details"][0]["description"]
        self.publisher=book_dict["book_details"][0]["publisher"]
        try:
            self.weeksOnList=int(book_dict["weeks_on_list"])
        except:
            self.weeksOnList=None
#print Book(getBestSellers("Mass-Market-Paperback")).weeksOnList
class BestSellerList():
    def __init__(self, listname, params={}):
        self.bestsellersDate=getBestSellers(listname)["results"][0]["bestsellers_date"]
        self.listname=getBestSellers(listname)["results"][0]["display_name"]
        self.bookList=[getBestSellers(listname)["results"][x]["book_details"][0]["title"] for x in range(getBestSellers(listname)["num_results"])]
#print BestSellerList("Mass-Market-Paperback").bookList
print "==2.1=="
lst=BestSellerList("E-Book-Fiction", {"date":"2015-11-08"})
get=getBestSellers("E-Book-Fiction", {"date":"2015-11-08"})["results"]
#for book in lst.bookList:
#    print str((lst.bookList).index(book)+1) + ". " + book + " by " + Book(get[lst.bookList.index(book)]).author + " (" + Book(get[lst.bookList.index(book)]).publisher +")"

print "==2.2=="
print pretty(searchBestSeller({"author":"Alice Munro", "title":"Dear Life"}))