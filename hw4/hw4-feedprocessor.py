######################################
# part 2: contributor counts

#Your next task is to extend the functionality from last week's homework
#assignment using functions.  In the last assignment, you read in the contents
#of a file, computed the poster contribution frequencies using a dictionary, and
#printed out the contents of that dictionary.

#This week, you'll write a function that computes the poster frequencies and
#returns the dictionary as a value. The dictionary will then be passed as a
#parameter to two more functions that will do something with it.

#### is_field() tests if the line represents the given kind of field
# you don't have to modify this function.
# inputs:
#   field_name: a string that specifies the kind of field
#   s: the string we are testing
# returns: True if s starts with field, False otherwise
def is_field(field_name, s):
    if s[:len(field_name)] == field_name:
        return True
    else:
        return False

#### contributor_counts() should return post and comment frequency for each user
# in the specified file
# input:
#   file_name: name of file being read
# returns: a dictionary of post and comment counts
# e.g., if we we had two posters, Alice (0 comments, 2 posts) and Bob (1 post, 2 comments), 
# our dictionary would look like
# cc = {"Alice":{"posts":2,"comments":0},"Bob":{"posts":1,"comments":2}}

# define contributor_counts() here.
# in this function, you MUST make at least one call to the function is_field

def contributor_counts(file_name):
    f = open(file_name, "r")
    cc = {}
    for line in f:
        if is_field("from", line) == True:
            person = line[6:-1]
            cc[person] = cc.get(person, {"posts":0, "comments":0})
        if is_field("post", line) == True:
            cc[person]["posts"] = cc[person]["posts"] + 1
        if is_field("comment", line) == True:
            cc[person]["comments"] = cc[person]["comments"] +1
    return cc

#### print_contributors() should print out the number of times each
# person posted and commented.
# The implementation should be similar to what was done in hw3 to print out
# contributor counts, but will use an if statement so that contributor counts of
# 1 will be printed as "once" and higher or zero counts will be "X times". For
# example:
#   "John Smith posted once and commented 2 times"
#   "Jane Smythe posted 4 times and commented 0 times"
#
# input parameter:
#   counts: dictionary of contributor counts

# define print_contributors() here.

def print_contributors(counts):
    for key in counts:
        if counts[key]["posts"] == 1 and counts[key]["comments"] == 1 :
            print key, "posted once and commented once"
        elif counts[key]["posts"] == 1 :
            print key, "posted once and commented", counts[key]["comments"], "times"
        elif counts[key]["comments"] == 1:
            print key, "posted", counts[key]["posts"], "times and commented once"
        else:
            print key, "posted", counts[key]["posts"], "times and commented", counts[key]["comments"], "times"

#### save_contributors() should save a comma separated value (CSV) formatted
# file where the first item is the key of a dictionary and the second item is
# the value (e.g. name,postcount,commentcount).
# input parameters:
#   counts: dictionary of contributor counts
#   output_file_name: name of the file being saved to

# the first line in the file should be a header row:
# name,post_count,comment_count 

# define save_contributors() here.

def save_contributors(counts, output_file_name):
    f = open(output_file_name, "w")
    f.write("name, postcount, commentcount")
    f.write("\n")
    for key in counts:
        f.write(key + "," + str(counts[key]["posts"]) + "," + str(counts[key]["comments"]))
        f.write("\n")
    f.close()


# the following code runs your functions to make sure they work properly
# uncomment all valid lines of python code to test your functions

# read in and count contributions
contributions = contributor_counts("hw4feed.txt")

# print the human readable version
print '------'
print_contributors(contributions)

# write the computer readable version
save_contributors(contributions, 'contributors.csv')


######################################
def stripWordPunctuation(word):
    return word.strip(".,()<>\"\\'~?!;*:[]-+")


######################################
# part 3: word counts

# To do this, we will write two functions. The first wordFreqs() which
# should take a file name as a parameter and generate a dictionary of
# words and their frequencies in posts and comments. 
#
# That is, the output should be a dictionary in the form:
# {'word':{'comments':3,'posts':5},'anotherword':{'comments':0,'posts':5}
# .... and so on. That is, it's another nested dictionary. 
#
# uncomment the next line and define wordFreqs() there

def wordFreqs(fname):
    f = open(fname, "r")
    s = open("stopwords.txt", "r")
    stopwords = []
    worddict = {}
    for word in s:
        stopwords.append(word[:-1])
    for line in f:
        if is_field("post", line) == True:
            line = line.split()
            for word in line[1:]:
                word = stripWordPunctuation(word).lower()
                if word not in stopwords:
                    worddict[word] = worddict.get(word, {"comments": 0, "posts":0})
                    worddict[word]["posts"] = worddict[word]["posts"] + 1
        if is_field("comment", line) == True:
            line = line.split()
            for word in line[1:]:
                word = stripWordPunctuation(word).lower()
                if word not in stopwords:
                    worddict[word] = worddict.get(word, {"comments": 0, "posts":0})
                    worddict[word]["comments"] = worddict[word]["comments"] + 1
    return worddict

# Next, we will write your writeFreqs() function, which takes, as a parameter
# a dictionary of the format output by wordFreqs() and writes a CSV file, freqs.csv.
# The first line of your file should be:
# word,postcount,commentcount
# and subsequent lines should contain the data.
#
# You must remove stopwords (the words in stopwords.txt). You may remove
# them in either wordFreqs() or in writeFreqs(). The decision is up to you.
# 
# uncomment the next line and define writeFreqs() there.

def writeFreqs(freqdict):
    f = open("freqs.csv", "w")
    f.write("word, postcount, commentcount \n")
    for key in freqdict:
        f.write(str(key) + "," + str(freqdict[key]["posts"]) + "," + str(freqdict[key]["comments"]) + "\n")
    f.close()
writeFreqs(wordFreqs("hw4feed.txt"))
