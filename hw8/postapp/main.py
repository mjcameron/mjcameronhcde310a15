#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2, urllib2, urllib, flickr_key, json
import jinja2

import os
import logging

JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

####### HW7 ########       
def safeGet(url):
    try:
        return urllib2.urlopen(url)
    except urllib2.HTTPError, e:
        print "The server couldn't fulfill the request." 
        print "Error code: ", e.code
    except urllib2.URLError, e:
        print "We failed to reach a server"
        print "Reason: ", e.reason
    return None

def flickrREST(baseurl = 'https://api.flickr.com/services/rest/',
    method = 'flickr.photos.search',
    api_key = 'ec3bec310497be7b61477f1678af1821',
    format = 'json',
    params={},
    printurl = False
    ):
    params['method'] = method
    params['api_key'] = api_key
    params['format'] = format
    if format == "json": params["nojsoncallback"]=True
    url = baseurl + "?" + urllib.urlencode(params)
    if printurl:
        print url
    else:
        return safeGet(url)
    
def get_photo_ids(tag, n=100):
    try:
        search = json.loads(flickrREST(params={"tags":tag,"per_page":n}).read())
        if len(search["photos"]["photo"]) > 0:
            return [search["photos"]["photo"][x]["id"] for x in range(n)]
    except:
        return None

def get_photo_info(photo_id):
    try:
        photo = json.loads(flickrREST(method = "flickr.photos.getInfo", params = {"photo_id":photo_id}).read())
        if "photo" in photo.keys():
            return photo["photo"]
    except:
        return None

class Photo():
    def __init__(self, photo_info):
        self.title = photo_info["title"]["_content"]
        self.author = photo_info["owner"]["username"]
        self.userid = photo_info["owner"]["nsid"]
        self.tags = [photo_info["tags"]["tag"][x]["_content"] for x in range(len(photo_info["tags"]["tag"]))]
        self.commentcount = photo_info["comments"]["_content"]
        self.num_views = photo_info["views"]
        self.url = photo_info["urls"]["url"][0]["_content"]
    def __str__(self):
        s = ''
        s += '~~~ ' + self.title.encode('utf-8') + ' ~~~\n'
        s += 'author: ' + self.author.encode('utf-8') + '\n'
        s += 'number of tags: ' + str(len(self.tags)) + '\n'
        s += 'views: ' + self.num_views.encode('utf-8') + '\n'
        s += 'comments: ' + self.commentcount.encode('utf-8')
        return s

def output(dic):
    thumbnail="https://farm%s.staticflickr.com/%s/%s_%s_t.jpg" %(str(dic["farm"]), str(dic["server"]), str(dic["id"]), str(dic["secret"]))
    pic_name=Photo(dic).title.encode('utf-8')
    pic_url=Photo(dic).url
    return thumbnail, pic_name, pic_url

class GreetHandler(webapp2.RequestHandler):
    def get(self):
        vals = {}
        vals['page_title']="Flickr Search"
        template = JINJA_ENVIRONMENT.get_template('greetform.html')
        self.response.write(template.render(vals))
    def post(self):
        vals = {}
        search = self.request.get('search', "husky")
        go = self.request.get('gobtn') 
        vals['page_title']="Search Results"
        logging.info(search)
        logging.info(go)
        try:
            # if form filled in, greet them using this data
            ids = get_photo_ids(search, n=5)
            vals['photos'] = [output(get_photo_info(x)) for x in ids]
            template = JINJA_ENVIRONMENT.get_template('greetresponse.html')
            self.response.write(template.render(vals))
            logging.info('search= '+search)
        except:
            #if not, then show the form again with a correction to the user
            vals['prompt'] = "What do you want to search?"
            template = JINJA_ENVIRONMENT.get_template('greetform.html')
            self.response.write(template.render(vals))
    

# for all URLs except alt.html, use MainHandler
application = webapp2.WSGIApplication([ \
                                      ('/.*', GreetHandler)
                                      ],
                                     debug=True)
