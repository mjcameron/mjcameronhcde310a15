import urllib, urllib2, json

def getFunding(keyword):
    baseurl = "http://api.donorschoose.org/common/json_feed.html?%s&APIKey=DONORSCHOOSE"
    url_keyword = {"keywords": keyword}
    url = urllib.urlencode(url_keyword)
    try:
        read = json.loads(urllib2.urlopen(baseurl % (url)).read())
        if read["searchTerms"] == keyword:
            return read
        else:
            print "Search not found"
    except urllib2.URLError, e:
        if hasattr(e,'reason'):
            print "We failed to reach a server"
            print "Reason", e.reason
        elif hasattr(e,"code"):
            print "The server couldn't fulfill the request."
            print "Error code: ", e.code

def printFundingProposals(keyword):
    if type(getFunding(keyword)) == dict:
        print str(len(getFunding(keyword)["proposals"]))+ " of " + getFunding(keyword)["totalProposals"] + ' total proposal results for "' + keyword + '":'
        for x in range(len(getFunding(keyword)["proposals"])):
            print "--- " + str(x+1) + " ---"
            print getFunding(keyword)["proposals"][x]["schoolName"].replace("&#039;", "'") + ": " + getFunding(keyword)["proposals"][x]["city"] + ", " + getFunding(keyword)["proposals"][x]["state"]
            print getFunding(keyword)["proposals"][x]["title"].replace("&#039;", "'")
            print getFunding(keyword)["proposals"][x]["fulfillmentTrailer"].replace("&#039;", "'")
            print getFunding(keyword)["proposals"][x]["percentFunded"]+ "% of $" + getFunding(keyword)["proposals"][x]["totalPrice"] + " has been funded"
        print ""
    else:
        return getFunding(keyword)
    

[printFundingProposals(x) for x in ["Seattle", "technology", "Ms. Thompson"]]

